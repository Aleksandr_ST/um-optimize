## Ultimate Member - Optimize

- Author: [Ultimate Member ream](https://ultimatemember.com/)
- Dependences: [WordPress](https://wordpress.org/), [Ultimate Member](https://wordpress.org/plugins/ultimate-member/)
- Version: 1.1.0

## Description

Optimize loading of the Ultimate Member pages and resources

## Key Features

- Removes CSS and JS files that belongs to Ultimate Member and its extensions if these files are not used on the page.

## Documentation

- [How to remove CSS and JS on non UM pages](https://docs.ultimatemember.com/article/1490-how-to-remove-css-and-js-on-non-um-pages)

## License

GNU Version 2 or Any Later Version

## Changelog

= 1.1.0: April 28, 2021 =

- Changed: The structure of the plugin
- Added: A new way to detect assets that belongs to UM extensions.

= 1.0.2: May 21, 2019 =

- Added: Verify if the page has UM widgets.

= 1.0.1: May 16, 2019 =

- Added: Remove CSS and JS that belongs to Ultimate Member on non-UM pages.